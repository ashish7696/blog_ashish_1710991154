package com.blog.backend.Controller;
import com.blog.backend.Model.Blogs;
import com.blog.backend.Repository.Blogrepository;
import com.blog.backend.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/blogs")
public class Blogcontroller {
    @Autowired
    Blogrepository br;

    @PostMapping("/addblog")
    public Blogs addblog(@Valid @RequestBody Blogs blog)
    {
        br.save(blog);
        return blog;
    }

    @GetMapping("/getblogs")
    public List<Blogs> getblogs()
    {
        return br.findAll();
    }
    @GetMapping("/getblog/{category}")
    public List<Blogs> getblogcategory(@PathVariable("category") String category)
    {
        return br.findAllByCategory(category);
    }

    @GetMapping("/getblogs/{username}")
    public List<Blogs> getblogussrname(@PathVariable("username") String user)
    {
        return br.findAllByUsername(user);
    }



    @GetMapping("/getblog/{username}/{category}")
    public List<Blogs> getbloguserandcat(@PathVariable(value="username") String user, @PathVariable(value="category") String category)
    {
        return br.findAllByUsernameAndCategory(user,category);
    }

    @DeleteMapping("/deleteblog/{id}")
    public ResponseEntity<?> deleteblog(@PathVariable(value ="id") Long blogid) {

        br.deleteById(blogid);
        return ResponseEntity.ok().build();
    }
    @PutMapping("/updateblog")
    public Blogs update(@Valid @RequestBody Blogs blog)
    {
        br.save(blog);
        return blog;
    }

}
