package com.blog.backend.Controller;

import com.blog.backend.Model.user;
import com.blog.backend.Repository.userRepository;
import com.blog.backend.exception.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    userRepository r;

    @GetMapping("/user-get")
    public List<user> getAll() {
        return r.findAll();
    }

    @GetMapping("/user-get/{id}")
    public user getuserByid(@PathVariable(value = "id") Long noteId) {
        return r.findById(noteId)
                .orElseThrow(() -> new ResourceNotFoundException("Note", "id", noteId));
    }

    @PostMapping("/userpost")
    public user create(@Valid @RequestBody user n) {
        return r.saveAndFlush(n);

    }

        @GetMapping("/user")
    public Optional<user> getuser(Principal principal){
        return r.findByUsername(principal.getName());
    }

//    @PutMapping(path = "/updateUserProfile", produces = "application/json")
//
//    public user updateUserProfile(@Valid @RequestBody user users, Principal principal) {
//        user use = r.findByUsername(principal.getName()).get();
//        use.setUsername(users.getUsername());
//        use.setPasssword(users.getPasssword());
////        user.setEmail(users.getEmail());
////        user.setPhone(users.setPhone());
//        r.saveAndFlush(users);
//        return use;
//    }
}


