package com.blog.backend.Controller;


import com.blog.backend.Model.Comments;
import com.blog.backend.Repository.commentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/comment")
public class commentController {

    @Autowired
    commentRepository  c;
    @PostMapping("/add")
    public Comments addComment(@Valid @RequestBody Comments comment, Principal principal)
    {
        c.save(comment);
        return comment;
    }
    @GetMapping("/fetch/{id}")
    public List<Comments> fetchComment(@PathVariable(value = "id")Long id)
    {
        return  c.findAllByBlog_Blogid(id);
    }
}
