package com.blog.backend.Controller;

import com.blog.backend.Model.followers;
import com.blog.backend.followerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("follower")
public class followerController {


        private com.blog.backend.followerService followerService;

        @Autowired
        public followerController(followerService followerService) {
            this.followerService = followerService;
        }

        @GetMapping(value = "/get-followers", produces = "application/json")
        public List<followers> getFollowers(Principal principal) {
            return followerService.getFollowers(principal);
        }

//        @GetMapping(value = "/followed", produces = "application/json")
//        public Boolean isFollowed(Principal principal, @RequestParam Long id) {
//            return followerService.isFollowed(principal, id);
//        }

        @PostMapping(value = "/follow/{id}", produces = "application/json")
        public List<followers> follow(Principal principal, @PathVariable(value="id") Long id) {
            return followerService.follow(principal, id);
        }

        @PostMapping(value = "/unfollow/{id}", produces = "application/json")
        public List<followers> unfollow(Principal principal,@PathVariable(value="id") Long id) {
            return followerService.unfollow(principal, id);
        }

        @GetMapping(value = "/following", produces = "application/json")
        public List<followers> following(Principal principal) {
            return followerService.following(principal);
        }




    }


