package com.blog.backend.Model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "userBlog")
public class userblog implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Blogs items;
    @ManyToOne
    private user users;

    public userblog() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Blogs getItems() {
        return items;
    }

    public void setItems(Blogs items) {
        this.items = items;
    }

    public user getUsers() {
        return users;
    }

    public void setUsers(user users) {
        this.users = users;
    }

}
