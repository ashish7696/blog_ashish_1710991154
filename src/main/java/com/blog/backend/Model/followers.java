package com.blog.backend.Model;

import javax.persistence.*;
import java.io.Serializable;
@Entity
public class followers implements Serializable {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id;

        @ManyToOne
        private user follower;

        @ManyToOne
        private user owner;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public user getFollower() {
            return follower;
        }

        public void setFollower(user follower) {
            this.follower = follower;
        }

        public user getOwner() {
            return owner;
        }

        public void setOwner(user owner) {
            this.owner = owner;
        }
    }


