package com.blog.backend.Model;
import  com.blog.backend.exception.ResourceNotFoundException;
import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;

import javax.persistence.ManyToOne;

@Entity
public class Comments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long commentId;

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public Comments() {
    }

    private  String users;
    private  String comment;
    @ManyToOne()
    private  Blogs blog;

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Comments(String users, String comment) {
        this.users = users;
        this.comment = comment;
    }
}
