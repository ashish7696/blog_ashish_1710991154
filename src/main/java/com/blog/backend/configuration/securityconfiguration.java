package com.blog.backend.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Configuration
public class securityconfiguration extends WebSecurityConfigurerAdapter
{
    @Autowired
    private DataSource ds;

    @Autowired
    public void globalSecurConfig(AuthenticationManagerBuilder auth) throws Exception
    {
        auth.
                jdbcAuthentication().dataSource(ds)
                .usersByUsernameQuery("select username, passsword, iactive from user where username=?")
                .authoritiesByUsernameQuery("select username,passsword, role from user where username =?");


    }
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception{
        httpSecurity.csrf().disable()
                .authorizeRequests().antMatchers(HttpMethod.OPTIONS,"/**").permitAll()
                .antMatchers("/signup").permitAll()
                .antMatchers("/signup/**").permitAll()
                .antMatchers("/home").permitAll()
                .antMatchers("/home/**").permitAll()
                .antMatchers("/login/**").permitAll()
                .antMatchers("/api/").permitAll()
                .antMatchers("/api/**").permitAll()
                .anyRequest().authenticated()
                .and().httpBasic();

        httpSecurity.cors();
    }
    @Bean
    public PasswordEncoder passwordEncoder()
    {
        return NoOpPasswordEncoder.getInstance();
    }
}
