package com.blog.backend;

import com.blog.backend.Model.user;
import com.blog.backend.Repository.userRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Optional;

@Service
public class Userservice {
    @Autowired
    userRepository ur;
    public Optional<user> getUser(Principal principal)
    {
        String username=principal.getName();
        return ur.findByUsername(username);
    }
}
