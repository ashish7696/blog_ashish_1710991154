package com.blog.backend.Repository;

import com.blog.backend.Model.Comments;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface commentRepository extends JpaRepository<Comments,Long> {
    public List<Comments> findAllByBlog_Blogid(Long id);
}
