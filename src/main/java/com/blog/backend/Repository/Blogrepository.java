package com.blog.backend.Repository;

import com.blog.backend.Model.Blogs;
import com.blog.backend.Model.user;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface Blogrepository extends JpaRepository<Blogs,Long> {


    List<Blogs>findAllByCategory(String cat);
    List<Blogs> findAllByUsername(String cat);
   List <Blogs>findAllByUsernameAndCategory(String user,String cat);
////    List<Blogs> findByPriceBetween(Double price, Double price1);
//    List<Blogs> findAll();
}
