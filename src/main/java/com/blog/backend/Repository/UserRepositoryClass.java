package com.blog.backend.Repository;

import com.blog.backend.Model.user;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class UserRepositoryClass {
    @Autowired
    userRepository UserRepository;
    public void add(user users) {
        System.out.println("Adding  a value");
        UserRepository.save(users);
    }
    public Optional<user> getByUsername(String username)
    {
        System.out.println("Getting by username");
        return UserRepository.findByUsername(username);
    }
    public Optional<user> getById (Long id) { return UserRepository.findById(id);}

}
