package com.blog.backend.Repository;

import com.blog.backend.Model.user;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;


public interface userRepository extends JpaRepository<user,Long> {
//    public List<user> findByUsername(String username);
    public Optional<user> findByUsername(String username);
    public Optional<user> findByUserId(Long userId);


}
