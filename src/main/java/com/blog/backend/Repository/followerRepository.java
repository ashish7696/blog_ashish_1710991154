package com.blog.backend.Repository;

import com.blog.backend.Model.followers;
import com.blog.backend.Model.user;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface followerRepository extends JpaRepository<followers, Long> {



        List<followers> findAllByOwner(user owner);
        //
        List<followers> findAllByFollower(user  follower);
        //
        Boolean existsByFollowerAndOwner(user subscriber, user Author);

        Optional<followers> findByFollowerAndOwner(user follower, user owner);


}
