package com.blog.backend;

import com.blog.backend.Model.followers;
import com.blog.backend.Model.user;
import com.blog.backend.Repository.followerRepository;
import com.blog.backend.Repository.userRepository;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.List;
import java.util.Optional;

@Service
public class followerService {


        private com.blog.backend.Repository.followerRepository followerRepository;
        private userRepository usersRepository;

        public followerService(followerRepository followerRepository, userRepository usersRepository) {
            this.followerRepository = followerRepository;
            this.usersRepository = usersRepository;
        }

        public List<followers> follow(Principal principal, Long id) {
            user follower = usersRepository.findByUsername(principal.getName()).get();
            user owner = usersRepository.findById(id).get();
            Optional<followers> optFollow = followerRepository.findByFollowerAndOwner(follower, owner);
            followers follow;
            if (!optFollow.isPresent() && !follower.equals(owner)) {
                follow = new followers();
                follow.setOwner(owner);
                follow.setFollower(follower);

                owner.setFollowers(owner.getFollowers() + 1);
                followerRepository.saveAndFlush(follow);
            }
            return followerRepository.findAllByOwner(follower);
        }

        public List<followers> unfollow(Principal principal, Long id) {
            user follower = usersRepository.findByUsername(principal.getName()).get();
            user owner = usersRepository.findById(id).get();
            Optional<followers> optFollow = followerRepository.findByFollowerAndOwner(follower, owner);

            owner.setFollowers(owner.getFollowers() - 1);
            optFollow.ifPresent(subscribers -> followerRepository.delete(subscribers));
            return followerRepository.findAllByFollower(follower);
        }

//
//        public Boolean isFollowed(Principal principal, Long id) {
//            user subscriber = usersRepository.findByUsername(principal.getName()).get();
//            user author = usersRepository.findById(id).get();
//            return followerRepository.existsByFollowerAndOwner(subscriber, author);
//        }

        public List<followers> following(Principal principal) {
            user user = usersRepository.findByUsername(principal.getName()).get();
            return followerRepository.findAllByFollower(user);
        }

        public List<followers> getFollowers(Principal principal) {
            user author = usersRepository.findByUsername(principal.getName()).get();
            return followerRepository.findAllByOwner(author);
        }


    }
